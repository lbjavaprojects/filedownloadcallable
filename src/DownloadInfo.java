
public class DownloadInfo {
	private long loadTime;
	private int fileSize;
	public DownloadInfo(long loadTime, int fileSize) {
		super();
		this.loadTime = loadTime;
		this.fileSize = fileSize;
	}
	public long getLoadTime() {
		return loadTime;
	}
	public int getFileSize() {
		return fileSize;
	}


}
