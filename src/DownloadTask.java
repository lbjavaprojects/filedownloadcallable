import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Callable;

public class DownloadTask implements Callable<DownloadInfo> {
	private String path;


	public DownloadTask(String path) {
		super();
		this.path = path;
	}
	@Override
	public DownloadInfo call() throws Exception {
        long timeStart=0,timeStop=0;
        int fileSize=0;
		try{
			timeStart=System.currentTimeMillis();
			URL url=new URL(path);
			try(InputStream is=url.openStream();
					FileOutputStream fos=new FileOutputStream(extractFileName())){
				int bufferSize=1024,c;
				byte[] buffer=new byte[bufferSize];

				while((c=is.read(buffer, 0, bufferSize))>-1){
					fos.write(buffer, 0, c);
					fileSize+=c;
				}
				System.out.println("Załadowano plik "+extractFileName());
                timeStop=System.currentTimeMillis();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new DownloadInfo(timeStop-timeStart,fileSize);
	}
	
	public String extractFileName(){
		return path.substring(path.lastIndexOf("/")+1);
	}

}
