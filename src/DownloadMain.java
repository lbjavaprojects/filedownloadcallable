import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class DownloadMain {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
	  ExecutorService pool=Executors.newSingleThreadExecutor();
	  DownloadTask task=new DownloadTask("https://gopro.com/content/dam/help/hero5-black/manuals/HERO5Black_UM_ENG_REVC_Web.pdf");
      Future<DownloadInfo> result= 
    		  pool.submit(task);
      pool.shutdown();
      if(pool.awaitTermination(10, TimeUnit.SECONDS)){
    	 System.out.println("Plik "+task.extractFileName()+" o rozmiarze "
      +result.get().getFileSize()+" zosta� za�adowany w ci�gu "+result.get().getLoadTime()
      +" milisekund"); 
      }else{
    	  System.out.println("Plik nie zosta� za�adowany w ci�gu 10 sekund");
      }
	}

}
